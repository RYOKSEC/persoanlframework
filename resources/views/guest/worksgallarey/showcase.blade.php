@extends('layouts.guest')
@section('content')
<br>

<div class="w-100 p-3 text-center">
<h1>{{$showwork->title}}</h1>
<img src="/work_picture/{{$showwork->picture}}" class="img-thumbnail img-responsive" />
<br>
<br>
<h4>{{$showwork->bio}}</h1>
</div>
<br>
<br>
<br>
  <div class="card gedf-card">
  <div class="card-header">
  <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
  <li class="nav-item">
  <a class="nav-link active" id="posts-tab" data-toggle="tab" href="#posts" role="tab" aria-controls="posts" aria-selected="true">Add a Comment</a>
  </li>
  </ul>    
  </div>
  <div class="card-body">
  <div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="posts" role="tabpanel" aria-labelledby="posts-tab">
  <div class="form-group">
  <label class="sr-only" for="message">post</label>

  {!! Form::open(['url' => '/guest/store/work/comment']) !!}
  <textarea class="form-control" name="comment" id="message" rows="3" placeholder="What are you thinking?"></textarea>
  </div>
  </div>
  <div class="btn-toolbar justify-content-between">
  <div class="btn-group">
  {{Form::submit('share',['class' => 'btn btn-primary '])}}
  </div>
  </div>
  </div>
  </div>
  </div>
  <span>{{Form::text('work_id', $showwork->id, ['class' => 'hide'])}}</span>
  {!! Form::close() !!}

@foreach($comments as $comment)

<div class="card">
    <div class="card-body">
        <div class="h5">{{$comment->body}}</div>
    </div>
  </div>
<br>
@endforeach
@endsection
