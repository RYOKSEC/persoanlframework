@extends('layouts.admin')
@section('content')

@if(count($posts) == null)
<h3>please create some posts</h3>
<br>
<a href="/admin/blog/create" class="btn btn-primary">Create a post</a>
@else
<br>
<a href="/admin/blog/create" class="btn btn-primary">Create a post</a>
<br>
<br>
@foreach($posts as $key => $post)
<div class="col mb-2">
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <h3 class="mb-0">
                <p class="text-dark" href="#">{{$post->title}}</p>
              </h3>
              <p class="card-text mb-auto">{{$post->body}}</p>
              @foreach($post->tags as $tag)
              <br>
              <strong>Tags : {{$tag->name}}</strong>
              @endforeach
              <br>
              <span><a href="/admin/blog/edit/{{$post->id}}" class="btn btn-success">Edit</a></span>
             </div>
             {!! Form::open(['action' => ['Admin_blog_controller@destroy_post', $post->id], 'method' => 'DELETE']) !!}
             {{Form::submit('Delete',['class' => 'btn btn-danger'])}}
           {!! Form::close() !!}
          </div>
        </div>
      </div>
@endforeach
@endif
@endsection
